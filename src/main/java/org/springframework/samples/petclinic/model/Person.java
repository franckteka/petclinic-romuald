package org.springframework.samples.petclinic.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Size;

/**
 * Simple JavaBean domain object representing an person.
 *
 */

@MappedSuperclass
public class Person extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "first_name")
	@Size(max = 30, message = "can not be more than 30 char")
	private String firstName;

	@Column(name = "last_name")
	@Size(max = 30, message = "can not be more than 30 char")
	private String lastName;

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}
