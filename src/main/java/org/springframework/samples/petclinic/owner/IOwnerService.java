package org.springframework.samples.petclinic.owner;

import java.util.Collection;

public interface IOwnerService {
	
	public void save(Owner owner);
	
	public void update(Owner owner);
	
	Collection<Owner> findByLastName(String lastName);
	
	Owner findById(Integer id);

}
