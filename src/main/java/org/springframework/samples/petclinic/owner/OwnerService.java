package org.springframework.samples.petclinic.owner;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OwnerService implements IOwnerService {

	@Autowired
	private OwnerRepository ownerRepository;

	@Override
	public void save(Owner owner) {
		Owner existingOwner = ownerRepository.findByFunctionalId(owner.getFunctionalId());
		if (null != existingOwner) {
			ownerRepository.updateLastDateByFunctionalId(new Date(), existingOwner.getFunctionalId());
		} else {
			ownerRepository.save(owner);
		}

	}
	
	@Override
	public void update(Owner owner) {
			ownerRepository.save(owner);
	}

	@Override
	public Collection<Owner> findByLastName(String lastName) {
		return ownerRepository.findByLastName(lastName);
	}

	@Override
	public Owner findById(Integer id) {
		return ownerRepository.findById(id);
	}
	
	
	
	

}
