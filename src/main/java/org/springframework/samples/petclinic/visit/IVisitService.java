package org.springframework.samples.petclinic.visit;

import java.util.List;

public interface IVisitService {
	
	public List<String> saveVisites(Integer petId, List<Visit> visit) ;
	
	List<Visit> findByPetId(Integer petId);

}
