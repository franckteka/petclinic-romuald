package org.springframework.samples.petclinic.pet;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PropertyComparator;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.samples.petclinic.model.NamedEntity;
import org.springframework.samples.petclinic.owner.Owner;
import org.springframework.samples.petclinic.treatment.Treatment;
import org.springframework.samples.petclinic.visit.Visit;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

/**
 * Simple business object representing a pet.
 *
 */

@Entity
@Table(name = "pets")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Pet extends NamedEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "birth_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate birthDate;

	@ManyToOne
	@JoinColumn(name = "type_id")
	private PetType type;

	@ManyToOne
	@JoinColumn(name = "owner_id")
	private Owner owner;
	
	@Lob
	@Column(name="report")
	private String treatmentReport;

	@Transient
	private Set<Visit> visits = new LinkedHashSet<>();

	@Transient
	private Set<Treatment> treatments = new LinkedHashSet<>();
	

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public LocalDate getBirthDate() {
		return this.birthDate;
	}

	public PetType getType() {
		return this.type;
	}

	public void setType(PetType type) {
		this.type = type;
	}

	public Owner getOwner() {
		return this.owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public Set<Visit> getVisitsInternal() {
		if (this.visits == null) {
			this.visits = new HashSet<>();
		}
		return this.visits;
	}

	public void setVisitsInternal(Collection<Visit> visits) {
		this.visits = new LinkedHashSet<>(visits);
	}

	public List<Visit> getVisits() {
		List<Visit> sortedVisits = new ArrayList<>(getVisitsInternal());
		PropertyComparator.sort(sortedVisits, new MutableSortDefinition("date", false, false));
		return Collections.unmodifiableList(sortedVisits);
	}

	public void addVisit(Visit visit) {
		getVisitsInternal().add(visit);
		visit.setPetId(this.getId());
	}

	protected Set<Treatment> getTreatmentsInternal() {
		if (this.treatments == null) {
			this.treatments = new HashSet<>();
		}
		return this.treatments;
	}

	protected void setTreatmentsInternal(Collection<Treatment> treatments) {
		this.treatments = new LinkedHashSet<>(treatments);
	}

	public List<Treatment> getTreatments() {
		List<Treatment> sortedTreatments = new ArrayList<>(getTreatmentsInternal());
		PropertyComparator.sort(sortedTreatments, new MutableSortDefinition("date", false, false));
		return Collections.unmodifiableList(sortedTreatments);
	}

	public void addTreatment(Treatment treatment) {
		getTreatmentsInternal().add(treatment);
		treatment.setPetId(this.getId());
	}

	public String getTreatmentReport() {
		return treatmentReport;
	}

	public void setTreatmentReport(String treatmentReport) {
		this.treatmentReport = treatmentReport;
	}

}
