package org.springframework.samples.petclinic.pet;

public interface IPetService {
	
	public Pet save(Pet pet);

}
