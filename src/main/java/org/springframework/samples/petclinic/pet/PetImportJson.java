package org.springframework.samples.petclinic.pet;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import org.springframework.samples.petclinic.owner.Owner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PetImportJson {

	@Valid
	private Owner owner;
	
	@Valid
	@NotEmpty
	private List <Pet> petList = new ArrayList<Pet>();
	
	
	public PetImportJson(Owner owner, List<Pet> petList) {
		super();
		this.owner = owner;
		this.petList = petList;
	}
	
	public PetImportJson() {
		super();
	}
	public Owner getOwner() {
		return owner;
	}
	public void setOwner(Owner owner) {
		this.owner = owner;
	}
	public List<Pet> getPetList() {
		return petList;
	}
	public void setPetList(List<Pet> petList) {
		this.petList = petList;
	}
}
