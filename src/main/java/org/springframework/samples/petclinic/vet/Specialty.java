package org.springframework.samples.petclinic.vet;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.samples.petclinic.model.NamedEntity;

/**
 * Models a {@link Vet Vet's} specialty (for example, dentistry).
 *
 */
@Entity
@Table(name = "specialties")
public class Specialty extends NamedEntity implements Serializable {

	private static final long serialVersionUID = 1L;

}
