package org.springframework.samples.petclinic.file;

public class FileImportException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private String msg;

    public FileImportException(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
